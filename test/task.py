#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import os
import unittest
from kirbybase import KirbyBase


class Task(object):
    def __init__(self, name):
        self.name = name


class TaskStore(object):
    def __init__(self, db):
        self.db = db
        self.tablename = 'tasks.table'
        self.schema = ['name:str']

        if not os.path.exists(self.tablename):
            self.db.create(self.tablename, self.schema)

    def save(self, task):
        return self.db.insert(self.tablename, task)

    def load(self, key):
        result = self.db.select(self.tablename, ['recno'], [key],
                                returnType='object')
        if not result:
            return None

        return result[0]


class Test_TaskStore(unittest.TestCase):
    def setUp(self):
        self.db = KirbyBase()
        self.db.drop('tasks.table')
        self.store = TaskStore(self.db)

    def test_save(self):
        self.store.save(Task('foo'))

        self.assert_('foo' in file('tasks.table').read())

    def test_load(self):
        key = self.store.save(Task('bar'))

        task = self.store.load(key)

        self.assertEqual(task.name, 'bar')

    def test_load_missing(self):
        value = self.store.load(1000)

        self.assertEqual(value, None)
